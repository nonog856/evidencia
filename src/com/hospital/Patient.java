package com.hospital;

public class Patient extends Human {
    public Patient(String fullName) {
        super(fullName);
    }

    @Override
    public String toString() {
        return "Id: " + this.getId() +
                "\nName: " + this.getFullName();
    }

}
