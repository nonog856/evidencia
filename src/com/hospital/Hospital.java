package com.hospital;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Hospital {

    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private User loggedInUser = null;
    private Doctor loggedInDoctor = null;


    public Hospital () {
        DB.syncDbFromFile();
        DB.syncDbToFile();
    }

    public void login() throws IOException {
        String username = null;
        String password = null;

        try {
            System.out.println(" /$$        /$$$$$$   /$$$$$$  /$$$$$$ /$$   /$$");
            System.out.println("| $$       /$$__  $$ /$$__  $$|_  $$_/| $$$ | $$");
            System.out.println("| $$      | $$  \\ $$| $$  \\__/  | $$  | $$$$| $$");
            System.out.println("| $$      | $$  | $$| $$ /$$$$  | $$  | $$ $$ $$");
            System.out.println("| $$      | $$  | $$| $$|_  $$  | $$  | $$  $$$$");
            System.out.println("| $$      | $$  | $$| $$  \\ $$  | $$  | $$\\  $$$");
            System.out.println("| $$$$$$$$|  $$$$$$/|  $$$$$$/ /$$$$$$| $$ \\  $$");
            System.out.println("|________/ \\______/  \\______/ |______/|__/  \\__/");
            System.out.println("");

            System.out.println("username");
            username = reader.readLine();

            System.out.println("password");
            password = reader.readLine();

            loggedInUser = DB.findUserByUserName(username);
            loggedInDoctor = DB.findDoctorByUserName(username);

            if (loggedInUser == null && loggedInDoctor == null) {
                System.out.println("User Does not exists please try again");
                this.login();
            }

            if (loggedInUser != null) {
                if (loggedInUser.getUserRole() == USER_ROLE.ADMINISTRATOR) {
                    if (Objects.equals(loggedInUser.getPassword(), password)) {
                        this.menu();
                        return;
                    }
                }
            } else if (loggedInDoctor != null) {
                if (loggedInDoctor.getUserRole() == USER_ROLE.ADMINISTRATOR) {
                    if (Objects.equals(loggedInDoctor.getPassword(), password)) {
                        this.menu();
                        return;
                    }
                }
            }

            this.login();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void menu () {
        try {
            if (loggedInUser != null) {
                System.out.println("Welcome " + loggedInUser.getFullName());
            }
            if (loggedInDoctor != null) {
                System.out.println("Welcome " + loggedInDoctor.getFullName());
            }

            String menu_option = "";
            do {
                System.out.println("[ 10 ] VIEW ALL DOCTORS		[ 20 ] VIEW ALL PATIENTS 	[ 30 ] VIEW ALL APPOINTMENTS 	[ 40 ] VIEW ALL USERS");
                System.out.println("[ 11 ] CREATE A DOCTOR		[ 21 ] CREATE A PATIENT 	[ 31 ] CREATE A APPOINTMENT 	[ 41 ] CREATE A USERS");
                System.out.println("[ 00 ] EXIT");
                System.out.println("SELECTION:");
                menu_option = reader.readLine();

                switch (Objects.requireNonNull(menu_option)) {
                    case "00":
                        System.exit(0);
                        break;
                    case "10":
                        this.readDoctors();
                        break;
                    case "11":
                        this.createDoctors();
                        break;
                    case "20":
                        this.readPatients();
                        break;
                    case "21":
                        this.createPatient();
                        break;
                    case "30":
                        this.readAppointments();
                        break;
                    case "31":
                        this.createAppointment();
                        break;
                    case "40":
                        this.readUsers();
                        break;
                    case "41":
                        this.createUser();
                        break;
                }
            } while (menu_option != null || menu_option != "00");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Doctor CRUD

    private void createDoctors () {
        try {
            String username = "";
            String password = "";
            String fullName = "";
            String specialization = null;
            String userRole = null;

            String fullNameRegEx = "[a-zA-z ]";
            Pattern fullNamePattern = Pattern.compile(fullNameRegEx);
            Matcher m = fullNamePattern.matcher(fullName);

            System.out.println("Doctor Information");

            while (!username.contains("@hospital.com")) {
                System.out.println("username : (hint: user@hospital.com)");
                username = reader.readLine();
            }

            while (password.length() <= 5) {
                System.out.println("password : (Hint min 5 characters)");
                password = reader.readLine();
            }

            while (!m.find()) {
                System.out.println("full name : Hint name lastname");
                fullName = reader.readLine();
                m = fullNamePattern.matcher(fullName);
            }

            while (!SPECIALIZATION.toStringList().contains(specialization)) {
                System.out.println("enter specialization : (Hint TRAUMA, GENERAL, DERMATOLOGY, ANESTHESIOLOGY)");
                specialization = reader.readLine();
            }

            while (!USER_ROLE.toStringList().contains(userRole)) {
                System.out.println("enter userRole : (Hint ADMINISTRATOR, DOCTOR, USER)");
                userRole = reader.readLine();
            }


            String[] attributes = new String[5];
            attributes[0] = username;
            attributes[1] = password;
            attributes[2] = fullName;
            attributes[3] = specialization;
            attributes[4] = userRole;

            Doctor doctor = DB.createDoctor(attributes);

            DB.addDoctor(doctor);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void readDoctors() {
        DB.printAllDoctors();
    }

    // Patient  CRUD
    private void createPatient () {
        try {
            String fullName = "";

            String fullNameRegEx = "[a-zA-z ]";
            Pattern fullNamePattern = Pattern.compile(fullNameRegEx);
            Matcher m = fullNamePattern.matcher(fullName);

            System.out.println("Patient Information");

            while (!m.find()) {
                System.out.println("full name : Hint name lastname");
                fullName = reader.readLine();
                m = fullNamePattern.matcher(fullName);
            }


            String[] attributes = new String[1];
            attributes[0] = fullName;

            Patient patient = DB.createPatient(attributes);
            DB.addPatient(patient);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void readPatients () {
        DB.printAllPatients();
    }

    // Appointment CRUD
    private void createAppointment () {
        try {
            String date = "";
            String reason = "";
            String doctorId = "";
            String patientId = "";

            String doctor_name = "";
            Doctor doctor = null;

            String patient_name = "";
            Patient patient = null;

            String dateRegEx = "(\\d{4})/(\\d{2})/(\\d{2}) (\\d{2}):(\\d{2})";
            Pattern datePattern = Pattern.compile(dateRegEx);
            Matcher dateMatch = datePattern.matcher(date);

            System.out.println("Appointment Information");

            while (!dateMatch.find()) {
                System.out.println("date : (Hint Date must be in format YYYY/MM/DD HH:mm 2021/12/31 14:30)");
                date = reader.readLine();
                dateMatch = datePattern.matcher(date);
            }

            while (reason.length() <= 5) {
                System.out.println("reason : (Hint min 5 characters)");
                reason = reader.readLine();
            }

            while (doctor == null) {
                System.out.println("lookup Doctor by name and assign to appointment");
                doctor_name = reader.readLine();

                doctor = DB.findDoctiorByName(doctor_name);
                if (doctor != null) {
                    System.out.println(doctor.toString());
                    doctorId = doctor.getId();
                } else {
                    System.out.println("Doctor not found");
                }

            }

            while (patient == null) {
                System.out.println("lookup Patient by name");
                patient_name = reader.readLine();

                patient = DB.findPatientByName(patient_name);
                if (patient != null) {
                    System.out.println(patient.toString());
                    patientId = patient.getId();
                } else {
                    System.out.println("Patient not found");
                }
            }

            String[] attributes = new String[4];
            attributes[0] = date;
            attributes[1] = reason;
            attributes[2] = doctorId;
            attributes[3] = patientId;

            Appointment appointment = DB.createAppointment(attributes);

            DB.addAppointment(appointment);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }


    }
    private void readAppointments () {
        DB.printAllAppointments();
    }

    // User CRUD
    private void createUser () {
        try {
            String username = "";
            String password = "";
            String fullName = "";
            String userRole = null;

            String fullNameRegEx = "[a-zA-z ]";
            Pattern fullNamePattern = Pattern.compile(fullNameRegEx);
            Matcher m = fullNamePattern.matcher(fullName);

            System.out.println("User Information");

            while (!username.contains("@hospital.com")) {
                System.out.println("username : (hint: user@hospital.com)");
                username = reader.readLine();
            }

            while (password.length() <= 5) {
                System.out.println("password : (Hint min 5 characters)");
                password = reader.readLine();
            }

            while (!m.find()) {
                System.out.println("full name : Hint name lastname");
                fullName = reader.readLine();
                m = fullNamePattern.matcher(fullName);
            }

            while (!USER_ROLE.toStringList().contains(userRole)) {
                System.out.println("enter userRole : (Hint ADMINISTRATOR, DOCTOR, USER)");
                userRole = reader.readLine();
            }

            String[] attributes = new String[4];
            attributes[0] = username;
            attributes[1] = password;
            attributes[2] = fullName;
            attributes[3] = userRole;

            User user = DB.createUser(attributes);

            DB.addUser(user);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void readUsers () {
        DB.printAllUsers();
    }
}
