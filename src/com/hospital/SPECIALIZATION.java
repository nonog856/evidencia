package com.hospital;

import java.util.ArrayList;
import java.util.List;

public enum SPECIALIZATION {
    TRAUMA,
    GENERAL,
    DERMATOLOGY,
    ANESTHESIOLOGY;

    public static List<String> toStringList() {
        List<String> values = new ArrayList<>();
        for (SPECIALIZATION specialization : SPECIALIZATION.values()) {
            values.add(specialization.toString());
        }
        return values;
    }

}
