package com.hospital;

import java.util.UUID;

public class Human {
    private final String id;
    private String fullName;

    public Human (String fullName) {
        this.id = String.valueOf(UUID.randomUUID());
        this.fullName = fullName;

    }

    public String getId() { return id; }
    public String getFullName() {
        return fullName;
    }
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
