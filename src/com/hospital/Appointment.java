package com.hospital;

import java.util.Date;
import java.util.UUID;

public final class Appointment {
    private final String id;
    private Date date;
    private String reason;
    private String doctorId;
    private final String patientId;

    public Appointment (Date date, String reason, String doctorId, String patientId) {
        this.id = String.valueOf(UUID.randomUUID());
        this.date = date;
        this.reason = reason;
        this.doctorId = doctorId;
        this.patientId = patientId;
    }

    public String formatAppointmentInfo(Doctor doctor, Patient patient) {
        return "Appoinment \n"
                + "Date: " + this.getDate().toString()
                + "\nReason: " + this.getReason()
                + "\nPatient: " + patient.getFullName()
                + "\nWith Doctor: " + doctor.getFullName()
                + "\nSpecialization: " + doctor.getSpecialization();
    }
    public String getId() { return id; }
    public Date getDate() { return date; }
    public void setDate(Date date) { this.date = date; }
    public String getReason() { return reason; }
    public void setReason(String reason) { this.reason = reason; }
    public String getDoctorId() { return doctorId; }
    public void setDoctorId(String doctorId) { this.doctorId = doctorId; }
    public String getPatientId() { return patientId; }
}
