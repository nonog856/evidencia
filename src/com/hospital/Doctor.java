package com.hospital;
import javax.swing.*;

public final class Doctor extends User {
    private SPECIALIZATION specialization;

    public Doctor(String username, String password, String fullName, SPECIALIZATION specialization, USER_ROLE userRole) {
        super(username, password, fullName, userRole);
        this.specialization = specialization;
    }

    public SPECIALIZATION getSpecialization() {
        return specialization;
    }

    public void setSpecialization(SPECIALIZATION specialization) {
        this.specialization = specialization;
    }

    @Override
    public String toString() {
        return "Id: " + this.getId() +
                "\nName: " + this.getFullName() +
                "\nSpecialization: " + this.getSpecialization();
    }

    public void update (String name, String lastName, SPECIALIZATION specialization ) {
        this.setFullName(name);
        this.setSpecialization(specialization);
    }
}
