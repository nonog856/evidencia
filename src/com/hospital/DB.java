package com.hospital;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class DB {

    // DB
    private static List<Doctor> doctors = new ArrayList<Doctor>();;
    private static List<Patient> patients = new ArrayList<Patient>();;
    private static List<Appointment> appointments = new ArrayList<Appointment>();;
    private static List<User> users = new ArrayList<User>();;

    private static final String DB_ROOT = "db/";
    private static final String USERS_FILE_NAME = "users.csv";
    private static final String DOCTORS_FILE_NAME = "doctors.csv";
    private static final String PATIENTS_FILE_NAME = "patients.csv";
    private static final String APPOINTMENTS_FILE_NAME = "appointments.csv";

    public static void syncDbFromFile() {
        syncUsersFromFile();
        syncDoctorsFromFile();
        syncPatientsFromFile();
        syncAppointmentsFromFile();
    }
    public static void syncUsersFromFile() {
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new FileReader(DB_ROOT + USERS_FILE_NAME));
            String line;
            if (!users.isEmpty()) {
                users = new ArrayList<User>();
            }
            while ((line = bufferedReader.readLine()) != null) {
                String[] _attributes = line.split(",");
                String [] attributes = Arrays.copyOfRange(_attributes, 1, _attributes.length);

                User user = createUser(attributes);

                users.add(user);
            }
            bufferedReader.close();
        } catch (IOException e) {
            System.out.println("IOException catched while reading: " + e.getMessage());
        }
    }
    public static void syncDoctorsFromFile() {
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new FileReader(DB_ROOT + DOCTORS_FILE_NAME));
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] _attributes = line.split(",");
                String [] attributes = Arrays.copyOfRange(_attributes, 1, _attributes.length);
                Doctor doctor = createDoctor(attributes);

                doctors.add(doctor);
            }
            bufferedReader.close();
        } catch (IOException e) {
            System.out.println("IOException catched while reading: " + e.getMessage());
        }
    }
    public static void syncPatientsFromFile() {
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new FileReader(DB_ROOT + PATIENTS_FILE_NAME));
            String line;
            if (!patients.isEmpty()) {
                patients = new ArrayList<Patient>();
            }
            while ((line = bufferedReader.readLine()) != null) {
                String[] _attributes = line.split(",");
                String [] attributes = Arrays.copyOfRange(_attributes, 1, _attributes.length);
                Patient patient = createPatient(attributes);
                patients.add(patient);
            }
            bufferedReader.close();
        } catch (IOException e) {
            System.out.println("IOException catched while reading: " + e.getMessage());
        }
    }
    public static void syncAppointmentsFromFile() {
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new FileReader(DB_ROOT + APPOINTMENTS_FILE_NAME));
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] _attributes = line.split(",");
                String [] attributes = Arrays.copyOfRange(_attributes, 1, _attributes.length);
                Appointment appointment = createAppointment(attributes);
                appointments.add(appointment);
            }
            bufferedReader.close();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    public static void syncDbToFile () {
        syncUsersToFile();
        syncDoctorsToFile();
        syncPatientsToFile();
        syncAppointmentsToFile();
    }
    public static void syncUsersToFile () {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(DB_ROOT + USERS_FILE_NAME));

            bufferedWriter.write("");
            for (User user : users) {
                String csv_line = user.getId() + "," + user.getUsername() + "," + user.getPassword() + "," + user.getFullName() + "," + user.getUserRole();
                bufferedWriter.write(csv_line);
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        } catch (IOException e) {
            System.out.println("IOException catched while writing: " + e.getMessage());
        }
    }
    public static void syncDoctorsToFile () {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(DB_ROOT + DOCTORS_FILE_NAME));
            for (Doctor doctor : doctors) {
                String csv_line = doctor.getId() + "," + doctor.getUsername() + "," + doctor.getPassword() + "," + doctor.getFullName() + "," + doctor.getSpecialization() + "," + doctor.getUserRole();
                bufferedWriter.write(csv_line);
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        } catch (IOException e) {
            System.out.println("IOException catched while writing: " + e.getMessage());
        }
    }
    public static void syncPatientsToFile () {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(DB_ROOT + PATIENTS_FILE_NAME));

            bufferedWriter.write("");
            for (Patient patient : patients) {
                String csv_line = patient.getId() + "," + patient.getFullName();
                bufferedWriter.write(csv_line);
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        } catch (IOException e) {
            System.out.println("IOException catched while writing: " + e.getMessage());
        }
    }
    public static void syncAppointmentsToFile () {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(DB_ROOT + APPOINTMENTS_FILE_NAME));

            bufferedWriter.write("");
            for (Appointment appointment : appointments) {

                String csv_line = appointment.getId() + "," + appointment.getReason() + "," + appointment.getDate() + "," + appointment.getDoctorId() + "," + appointment.getPatientId();
                bufferedWriter.write(csv_line);
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        } catch (IOException e) {
            System.out.println("IOException catched while writing: " + e.getMessage());
        }
    }


    /*
    * D O C T O R S
    * */

    public static void addDoctor (Doctor doctor) throws IOException {
        doctors.add(doctor);
        syncDbToFile();
    }
    public static List<Doctor> getAllDoctors () {
        return doctors;
    }
    public static Doctor findDoctor (String id) {
        return doctors.stream()
                .filter(doctor -> id.equals(doctor.getId()))
                .findAny()
                .orElse(null);
    }
    public static Doctor findDoctorByUserName(String username) {
        return doctors.stream()
                .filter(doctor -> username.equals(doctor.getUsername()))
                .findAny()
                .orElse(null);
    }
    public static Doctor findDoctiorByName(String doctor_name) {
        return doctors.stream()
                .filter(doctor -> doctor_name.toLowerCase().equals(doctor.getFullName().toLowerCase()))
                .findAny()
                .orElse(null);
    }
    public static Doctor createDoctor(String[] attributes) {
        String username = attributes[0];
        String password = attributes[1];
        String fullName = attributes[2];
        SPECIALIZATION specialization = SPECIALIZATION.valueOf(attributes[3]);
        USER_ROLE userRole = USER_ROLE.valueOf(attributes[4]);
        return new Doctor(username, password, fullName, specialization, userRole);
    }
    public static void printAllDoctors(){
        doctors.forEach(doctor -> System.out.println(doctor.toString()));
    }


    /*
     * P A T I E N T S
     * */
    public static void addPatient(Patient patient) throws IOException {
        patients.add(patient);
        syncDbToFile();
    }
    public static List<Patient> getAllPatients() {
        return patients;
    }
    public static Patient findPatient(String id) {
        return patients.stream()
                .filter(patient -> id.equals(patient.getId()))
                .findAny()
                .orElse(null);
    }
    public static Patient createPatient(String[] attributes) {
        String fullName = attributes[0];
        return new Patient(fullName);
    }
    public static void printAllPatients(){
        patients.forEach(patient -> System.out.println(patient.toString()));
    }
    public static Patient findPatientByName(String patient_name) {
        return patients.stream()
                .filter(patient -> patient_name.toLowerCase().equals(patient.getFullName().toLowerCase()))
                .findAny()
                .orElse(null);
    }

    /*
     * A P P O I N T M E N T S
     * */
    public static void addAppointment (Appointment appointment) throws IOException {
        appointments.add(appointment);
        syncDbToFile();
    }
    public static List<Appointment> getAllAppointment () {
        return appointments;
    }
    public static Appointment findAppointment (String id) {
        return appointments.stream()
                .filter(appointment -> id.equals(appointment.getId()))
                .findAny()
                .orElse(null);
    }
    public static Appointment createAppointment(String[] attributes) throws ParseException {
        String _date = attributes[0];
        Date date = new SimpleDateFormat("yyyy/MM/DD HH:mm").parse(_date);
        String reason = attributes[1];
        String doctorId = attributes[2];
        String patientId = attributes[3];

        return new Appointment(date, reason, doctorId, patientId);
    }
    public static void printAllAppointments(){
        appointments.forEach(appointment -> {
            Doctor doctor = findDoctor(appointment.getDoctorId());
            Patient patient = findPatient(appointment.getPatientId());
            System.out.println(appointment.formatAppointmentInfo(doctor, patient));
        });
    }

    /*
     * U S E R S
     * */
    public static void addUser (User user) throws IOException {
        users.add(user);
        syncDbToFile();
    }
    public static List<User> getAllUsers () {
        return users;
    }
    public static User findUser (String id) {
        return users.stream()
                .filter(user -> id.equals(user.getId()))
                .findAny()
                .orElse(null);
    }
    public static User findUserByUserName(String username) {
        return users.stream()
                .filter(user -> username.toLowerCase().equals(user.getUsername().toLowerCase()))
                .findAny()
                .orElse(null);
    }
    public static User createUser(String[] attributes) {
        String username = attributes[0];
        String password = attributes[1];
        String fullName = attributes[2];
        USER_ROLE userRole = USER_ROLE.valueOf(attributes[3]);
        return new User(username, password, fullName, userRole);
    }
    public static void printAllUsers(){
        users.forEach(users -> System.out.println(users.toString()));
    }
}
