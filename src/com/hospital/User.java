
package com.hospital;

import java.util.Date;

public class User extends Human {
    private String username;
    private String password;
    private USER_ROLE userRole;
    private Date session = null;

    public User (String username, String password, String fullName, USER_ROLE userRole) {
        super(fullName);
        this.username = username;
        this.password = password;
        this.userRole = userRole;
    }
    public User (String username, String password, String fullName) {
        super(fullName);
        this.username = username;
        this.password = password;
        this.userRole = USER_ROLE.USER;
    }




    public String getUsername() {
        return username;
    }
    public void setUsername(String username) { this.username = username; }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public Date getSession() { return session; }
    public void setSession(Date session) { this.session = session; }
    public USER_ROLE getUserRole() {
        return userRole;
    }
    public void setUserRole(USER_ROLE userRole) {
        this.userRole = userRole;
    }

    @Override
    public String toString() {
        return "Id: " + this.getId() +
                "username" + this.getUsername() +
                "\nName: " + this.getFullName();
    }

}
