package com.hospital;

import java.util.ArrayList;
import java.util.List;

public enum USER_ROLE {
    ADMINISTRATOR,
    DOCTOR,
    USER;


    public static List<String> toStringList() {
        List<String> values = new ArrayList<>();
        for (USER_ROLE specialization : USER_ROLE.values()) {
            values.add(specialization.toString());
        }
        return values;
    }

}
